# Dynamic logger Implementation

[support](support)

## Configuration
* configuration file [configuration.py](support/configuration.py)
* python code setting example configuration [app_logger.py](support/app_logger.py)
```commandline
import logging.config
from .configuration import LOGGER_SETTINGS

logging.config.dictConfig(LOGGER_SETTINGS)
# logger = logging.getLogger('automation')
logger = logging.getLogger('production')
```

## usage

```commandline
from support.app_logger import logger

logger.info("--- Generic ---")
logger.debug("--- Generic ---")
logger.warning("--- Generic ---")
logger.error("--- Generic ---")
logger.exception("--- Generic ---")
```

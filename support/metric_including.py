import os
from opentelemetry.sdk.resources import SERVICE_NAME
from opentelemetry.metrics import Counter
from dotenv import load_dotenv
from providers import SamMeterProvider

load_dotenv()

PROVIDER = None


def metric_counter(counter_name: str, counter_description: str) -> Counter:
    global PROVIDER

    def setting_provider():
        schema_url = "localhost:4317"
        service_name = os.getenv('METRIC_PROVIDER_NAME')
        attributes = {
            SERVICE_NAME: service_name
        }
        return SamMeterProvider(attributes=attributes, schema_url=schema_url)

    if PROVIDER is None:
        PROVIDER = setting_provider()

    _meter = PROVIDER.get_meter(counter_name)
    return _meter.create_counter(name=counter_name, description=counter_description)

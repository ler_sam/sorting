LOGGER_SETTINGS = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(asctime)s %(message)s'
        },
    },
    'handlers': {
        'null': {
            'class': 'logging.NullHandler',
            'formatter': 'simple'
        },
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
        },
        'file': {
            'class': 'logging.FileHandler',
            'filename': '/tmp/automation.log',
            'formatter': 'verbose',
        },
    },
    'loggers': {
        'automation': {
            'level': 'DEBUG',
            'handlers': ['file', 'console'],
        },
        'production': {
            'level': 'INFO',
            'handlers': ['file'],
        },
    }
}

from support.metric_including import metric_counter


def selection_sort(input_val):
    """
    Selection sort — O(n2)

    Find the minimal element and swap it with the first element of an array. Next,
    just sort the rest of the array, without the first element, in the same way.
    Notice that after k iterations (repetition of everything inside the loop) the first k elements
    will be sorted in the right order (this property type called the loop invariant)
    """

    global_counter = metric_counter(counter_name="global_counter", counter_description="selection_sort main loop")
    sub_counter = metric_counter(counter_name="sub_counter", counter_description="selection_sort sub loop")

    n = len(input_val)
    for k in range(n):
        minimal = k
        global_counter.add(1)
        for j in range(k + 1, n):
            sub_counter.add(1)
            if input_val[j] < input_val[minimal]:
                minimal = j

        # swap input[k] and input[minimal]
        input_val[k], input_val[minimal] = input_val[minimal], input_val[k]
    return input_val

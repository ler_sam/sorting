import logging.config
from .configuration import LOGGER_SETTINGS

logging.config.dictConfig(LOGGER_SETTINGS)
# logger = logging.getLogger('automation')
logger = logging.getLogger('production')

import random

from sorting_algorithms.counting_sort import counting_sort
from sorting_algorithms.generic import natural_number_sum
from sorting_algorithms.selection_sort import selection_sort
from support.app_logger import logger

if __name__ == "__main__":
    total_numbers = 10
    random_range = 100
    in_array = [random.randint(1, random_range) * (i + 1) for i in range(total_numbers)]
    logger.info("input array: {}".format(in_array))

    sorted_output_array = selection_sort(in_array)
    counting_output_array = counting_sort(in_array)

    logger.info("'Selection sort' result: {}".format(sorted_output_array))
    logger.info("'Counting sort'  result: {}".format(counting_output_array))

    logger.info("--- Generic ---")
    result = natural_number_sum(6)
    logger.info("'Natural number sum' result: {}".format(result))

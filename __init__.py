import sys
import os

dir_path = os.path.dirname(__file__)

if dir_path not in sys.path:
    sys.path.append(dir_path)

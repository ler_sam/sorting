from support.metric_including import metric_counter


def counting_sort(input_val: list) -> list:
    """
    Counting sort — O(n + k)

    First, count the elements in the array of counters (see chapter 2). Next, just iterate
    through the array of counters in increasing order.
    Notice that we have to know the range of the sorted values. If all the elements are in the
    set {0, 1, . . . , k}, then the array used for counting should be of size k+1. The limitation here
    may be available memory.
    """
    global_counter = metric_counter(counter_name="counting_sort_main", counter_description="counting_sort main loop")
    sub_counter = metric_counter(counter_name="counting_sort_while", counter_description="selection_sort sub loop")
    count = [0] * (max(input_val) + 1)
    for index in input_val:
        count[index] += 1

    result = []
    for index, counter in enumerate(count):
        global_counter.add(1)
        while counter > 0:
            sub_counter.add(1)
            result.append(index)
            counter -= 1

    return result

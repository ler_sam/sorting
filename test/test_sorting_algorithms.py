import pytest
from sorting_algorithms.selection_sort import selection_sort
from sorting_algorithms.counting_sort import counting_sort


@pytest.mark.parametrize("in_array, expected", [
    ([3, 154, 243, 252, 45, 288, 175, 176, 711, 470],
     [3, 45, 154, 175, 176, 243, 252, 288, 470, 711])])
def test_selection_sort(in_array, expected):
    assert expected == selection_sort(in_array)


@pytest.mark.parametrize("in_array, expected", [
    ([3, 154, 243, 252, 45, 288, 175, 176, 711, 470],
     [3, 45, 154, 175, 176, 243, 252, 288, 470, 711])])
def test_counting_sort(in_array, expected):
    assert expected == counting_sort(in_array)

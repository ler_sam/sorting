def natural_number_sum(n: int) -> int:
    """
    Sum of the First n Natural Numbers
    The formula 1+ 2+ ... + n = n(n+1) / 2, for n a natural number.
    """
    return n * (n + 1) / 2

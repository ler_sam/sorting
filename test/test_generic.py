from sorting_algorithms.generic import natural_number_sum


def test_natural_number_sum():
    assert natural_number_sum(5) == 15
